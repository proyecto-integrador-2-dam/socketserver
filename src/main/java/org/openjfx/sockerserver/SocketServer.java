package org.openjfx.sockerserver;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.URL;
import java.time.LocalDate;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyMap;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;

/**
 *
 * @author JSA
 */
public class SocketServer {

    private static final int PORT = 9000;
//    private static final int PORT = 2222;

    public static void main(String[] args) {
        System.out.println("********************************************");
        System.out.println("**********SERVER UBICATION SERVICE**********");
        System.out.println("********************************************");

        try (DatagramSocket socket = new DatagramSocket(PORT)) {

            System.out.println("<<WAITING FOR CLIENT CONNECTION>>");

            while (true) {
                byte[] message = new byte[50];
                DatagramPacket datagramPacket = new DatagramPacket(message, message.length);

                socket.receive(datagramPacket);
                System.out.println("---NEW CLIENT CONNECTED---");
                new UbicationThread((datagramPacket)).start();
            }

        } catch (SocketException ex) {
//            System.out.println("\nSOCKET ERROR: " + ex.getMessage());
            ex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
//            System.out.println("General Error: " + ex.getMessage());
        }

    }

    public static class UbicationThread extends Thread {

        private DatagramPacket datagramPacket;
        private InetAddress senderAddr;
        private int senderPort;
        private int idDeliveryMan;
        private double lat;
        private double lon;

        private static XmlRpcClient client;
        public static final String DB = "batoilogic";
        public static final String USER = "grup3@batoilogic.es";
        public static final String PWD = "grup3-3333";
        public static int uid;

        public static XmlRpcClient getInstance() throws MalformedURLException, XmlRpcException {
            if (client == null) {
                client = new XmlRpcClient();

                XmlRpcClientConfigImpl configBD = new XmlRpcClientConfigImpl();
                configBD.setEnabledForExtensions(true);
                configBD.setServerURL(new URL("http", "localhost", 8069, "/xmlrpc/2/object"));

                client.setConfig(configBD);

                XmlRpcClientConfigImpl auth = new XmlRpcClientConfigImpl();
                auth.setEnabledForExtensions(true);
                auth.setServerURL(new URL("http", "localhost", 8069, "/xmlrpc/2/common"));

                uid = (int) client.execute(auth, "authenticate", asList(DB, USER, PWD, emptyMap()));
            }

            return client;
        }

        public UbicationThread(DatagramPacket datagramPacket) {
            this.datagramPacket = datagramPacket;
            this.senderAddr = datagramPacket.getAddress();
            this.senderPort = datagramPacket.getPort();

            String[] packageData = new String(datagramPacket.getData()).trim().split(":");
            this.idDeliveryMan = Integer.parseInt(packageData[0]);
            this.lat = Double.parseDouble(packageData[1]);
            this.lon = Double.parseDouble(packageData[2]);
        }

        @Override
        public void run() {
            //do a synchronized method to update the ubication in the db

            synchronized (this) {
                try {
                    final LocalDate d = LocalDate.now();
                    client = getInstance();
                    final Integer id = (Integer) client.execute("execute_kw", asList(
                            DB, uid, PWD,
                            "batoi_logic.ubicacion", "create",
                            asList(new HashMap() {
                                {
                                    put("fecha", String.valueOf(d));
                                    put("latitud", lat);
                                    put("longitud", lon);
                                    put("repartidor", idDeliveryMan);
                                    
                                }
                            })
                    ));
                } catch (XmlRpcException ex) {
                    Logger.getLogger(SocketServer.class.getName()).log(Level.SEVERE, null, ex);
                } catch (MalformedURLException ex) {
                    Logger.getLogger(SocketServer.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            System.out.println("--- LAT AND LON STORED IN THE DATABASE---\nLAT: " + lat + "\nLON: " + lon + "\nFROM DeliveryMan: " + idDeliveryMan);
        }
    }

}
